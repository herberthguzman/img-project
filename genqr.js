/* ************************
 genQR.js
 Por: Omar Moreno
 msdesarrollo21@gmail.com
 Fecha: 15-06-2021
 V: 0.001
************************** */
//datox: recibe la cadena de texto que va mostrar en el QR.
//idx: es el id del objeto (webcontainer de gambas3) usado para mostrar el QR.

function genQR(datox, labelx, idx)
{
  document.getElementById(idx).innerHTML = "";
  var qrk = kjua({
              text: datox, 
              render: "canvas", 
              crisp: true, 
              size: 250,
              mode: 'label',
              mSize: 7,
              mPosX: 50,
              mPosY: 50,
              label: labelx,
              fontname: 'sans',
              fontcolor: '#d1751f'
          });
  document.getElementById(idx).appendChild(qrk);
}
